package org.example.simulation.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.simulation.dto.configuration.SimulationConfiguration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ConfigurationLoader {
    public static SimulationConfiguration loadConfiguration(String filePath) {
        try {
            byte[] jsonData = Files.readAllBytes(Paths.get(filePath));
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(jsonData, SimulationConfiguration.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
