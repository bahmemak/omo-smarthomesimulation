package org.example.simulation.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class ReportExportUtil {
    public static void exportReport(String report, String fileName) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(new File(fileName), report);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
