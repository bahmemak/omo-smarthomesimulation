package org.example.simulation.report;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.simulation.house.floor.room.Human;
import org.example.simulation.house.floor.room.device.IDevice;

import java.util.HashMap;
import java.util.Map;

public class ActivityAndUsageReport {
    private static ActivityAndUsageReport instance;

    private Map<Human, Map<Integer, Integer>> activityAndUsageReport = new HashMap<>();

    public static ActivityAndUsageReport getInstance() {
        if (instance == null) {
            instance = new ActivityAndUsageReport();
        }
        return instance;
    }

    public void addUsage(Human human, int deviceId) {
        Map<Integer, Integer> usage = activityAndUsageReport.get(human);
        if (usage == null) {
            usage = new HashMap<>();
        }
        usage.put(deviceId, usage.getOrDefault(deviceId, 0) + 1);
        activityAndUsageReport.put(human, usage);
    }

    public String getReport() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(activityAndUsageReport);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
