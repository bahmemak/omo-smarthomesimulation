package org.example.simulation.report;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.simulation.dto.resourceConsumption.ResourceConsumptionDetails;

import java.util.HashMap;
import java.util.Map;

public class ConsumptionReport {
    private static ConsumptionReport instance;

    public static ConsumptionReport getInstance() {
        if (instance == null) {
            instance = new ConsumptionReport();
        }
        return instance;
    }

    private Map<Integer, ResourceConsumptionDetails> consumption = new HashMap<>(); // map id -> consumption

    public void addDevice(int id) {
        this.consumption.put(id, new ResourceConsumptionDetails());
    }

    public void addElectricityConsumption(int id, int consumption) {
        ResourceConsumptionDetails details = this.consumption.get(id);
        details.setElectricityConsumption(
                details.getElectricityConsumption()
                        + consumption);
    }

    public void addWaterConsumption(int id, int consumption) {
        ResourceConsumptionDetails details = this.consumption.get(id);
        details.setWaterConsumption(details.getWaterConsumption() + consumption);
    }

    public void addGasConsumption(int id, int consumption) {
        ResourceConsumptionDetails details = this.consumption.get(id);
        details.setGasConsumption(details.getGasConsumption() + consumption);
    }

    public void addDurabilityConsumption(int id, int consumption) {
        ResourceConsumptionDetails details = this.consumption.get(id);
        if (details == null) {
            details = new ResourceConsumptionDetails();
            this.consumption.put(id, details);
        }
        details.setDurabilityConsumption(details.getDurabilityConsumption() + consumption);
    }

    public String getReport() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(consumption);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
