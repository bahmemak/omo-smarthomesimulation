package org.example.simulation;

import lombok.*;
import org.example.simulation.dto.Documentation;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Getter
@Setter
public class DocumentationRepository {
    private static Map<Integer, Documentation> documentation = new HashMap<>();

    public static Documentation getDocumentation(int id) {
        return documentation.get(id);
    }
}
