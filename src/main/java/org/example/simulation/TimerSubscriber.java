package org.example.simulation;

public interface TimerSubscriber {
    void update(int time);
}
