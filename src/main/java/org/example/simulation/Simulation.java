package org.example.simulation;

import lombok.*;
import org.example.simulation.dto.configuration.SimulationConfiguration;
import org.example.simulation.dto.resourceConsumption.ResourceConsumptionPerState;
import org.example.simulation.event.EventManager;
import org.example.simulation.house.House;
import org.example.simulation.house.floor.room.device.Device;
import org.example.simulation.house.floor.room.device.deviceConsumption.deviceConsumptionDecorator.BaseConsumptionDecorator;
import org.example.simulation.house.floor.room.device.deviceState.DeviceState;
import org.example.simulation.house.floor.room.device.deviceState.DisabledState;
import org.example.simulation.report.ActivityAndUsageReport;
import org.example.simulation.report.ConsumptionReport;
import org.example.simulation.util.ReportExportUtil;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Simulation {
    private int duration;

    private House house;

    private Timer timer;

    private EventManager eventManager;

    public Simulation(int duration) {
        this.duration = duration;
    }

    public void run() {
        while (Timer.getTime() < duration) {
            timer.incrementTime();
        }

        ReportExportUtil.exportReport(ConsumptionReport.getInstance().getReport(), "resourceConsumptionReport.json");
        ReportExportUtil.exportReport(ActivityAndUsageReport.getInstance().getReport(), "activityAndUsageReport.json");
    }
}
