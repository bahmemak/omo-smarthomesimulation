package org.example.simulation.house.floor.room.device.deviceConsumption;

public enum EResourceType {
    DURABILITY,
    ELECTRICITY,
    WATER,
    GAS
}
