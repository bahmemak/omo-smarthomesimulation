package org.example.simulation.house.floor.room.sensor;

import lombok.*;
import org.example.simulation.util.RandomGenerator;
import org.example.simulation.event.EventManager;
import org.example.simulation.event.eventCommand.EventCommand;
import org.example.simulation.event.eventCommand.UseCommand;
import org.example.simulation.house.House;

@AllArgsConstructor
@Setter
@Getter
public class TemperatureSensor extends Sensor {
}
