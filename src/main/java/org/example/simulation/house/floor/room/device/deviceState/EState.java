package org.example.simulation.house.floor.room.device.deviceState;

public enum EState {
    ENABLED,
    DISABLED,
    ACTIVE,
    BROKEN
}
