package org.example.simulation.house.floor.room.device;

import org.example.simulation.TimerSubscriber;
import org.example.simulation.house.floor.room.device.deviceState.EState;

public interface IDevice extends TimerSubscriber {
    void enable();
    void use();
    void disable();
    void repair();
    int getId();

    EState getState();

    EDevice getType();

    boolean isIntractableWithHumans();

    String getDeviceName();


}
