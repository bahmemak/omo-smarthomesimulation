package org.example.simulation.house.floor.room.device;

public enum EDevice {
    HEATER,
    CONDITIONER,
    OUTDOOR_BLINDS,
    WINDOW,
    LIGHT,
    REFRIGERATOR,
    OVEN,
    WASHING_MACHINE,
    VACUUM_CLEANER,
    DISHWASHER,
    TV,
    COFFEE_MACHINE
}
