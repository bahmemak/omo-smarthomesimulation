package org.example.simulation.house.floor.room.device.deviceState;

import lombok.*;
import org.example.simulation.house.floor.room.device.Device;
import org.example.simulation.house.floor.room.device.EDevice;
import org.example.simulation.house.floor.room.device.IDevice;

@Setter
@Getter
public abstract class DeviceState implements IDevice {
    private Device device;

    public DeviceState(Device device) {
        this.device = device;
    }

    public DeviceState() {
    }

    @Override
    public EDevice getType() {
        return device.getType();
    }

    @Override
    public boolean isIntractableWithHumans() {
        return device.isIntractableWithHumans();
    }

    @Override
    public String getDeviceName() {
        return device.getDeviceName();
    }

    @Override
    public int getId() {
        return device.getId();
    }
}
