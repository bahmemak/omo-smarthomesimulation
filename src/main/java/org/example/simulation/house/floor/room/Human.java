package org.example.simulation.house.floor.room;

import lombok.*;
import org.example.simulation.TimerSubscriber;
import org.example.simulation.house.floor.Floor;
import org.example.simulation.house.floor.room.device.IDevice;
import org.example.simulation.report.ActivityAndUsageReport;
import org.example.simulation.util.RandomGenerator;
import org.example.simulation.house.floor.room.device.deviceState.EState;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Human implements TimerSubscriber {
    private int id;

    private Room room;

    private IDevice device;

    private String name;

    @Override
    public void update(int time) {
        if (device == null) {
            switch (RandomGenerator.generateRandomNumber(0, 1)) {
                case 0:
                    changeRoom();
                    break;
                case 1:
                    room.getDevices().stream()
                            .filter(device -> device.isIntractableWithHumans() && (device.getState() != EState.ACTIVE))
                            .findAny()
                            .ifPresent(device -> {
                                if (device.getState() == EState.BROKEN) {
                                    device.repair();
                                    this.device = device;
                                    ActivityAndUsageReport.getInstance().addUsage(this, device.getId());
                                    return;
                                }
                                if (device.getState() == EState.DISABLED) {
                                    this.device = device;
                                    device.enable();
                                }
                                this.device = device;
                                ActivityAndUsageReport.getInstance().addUsage(this, device.getId());
                                device.use();
                            });
                    break;
            }
        } else if (device.getState() != EState.ACTIVE) {
            device = null;
        }
    }

    public void changeRoom() {
        room.getHumans().remove(this);

        List<Floor> floors = room.getFloor().getHouse().getFloors();

        Floor floor = floors.get(RandomGenerator.generateRandomNumber(0, floors.size()-1));

        Room room = floor.getRooms().get(RandomGenerator.generateRandomNumber(0, floor.getRooms().size()-1));

        this.room = room;
        room.getHumans().add(this);
    }
}
