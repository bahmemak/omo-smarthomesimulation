package org.example.simulation.house.floor.room.device.deviceState;

import lombok.*;
import org.example.simulation.DocumentationRepository;
import org.example.simulation.house.floor.room.device.Device;

@AllArgsConstructor
@Setter
@Getter
public class BrokenState extends DeviceState {
    public BrokenState(Device device) {
        super(device);
    }

    @Override
    public void update(int time) {
        System.out.println("BrokenState update");
    }

    @Override
    public void enable() {
        System.out.println("BrokenState enable");
    }

    @Override
    public void use() {
        System.out.println("BrokenState use");
    }

    @Override
    public void disable() {
        System.out.println("BrokenState disable");
    }

    @Override
    public void repair() {
        super.getDevice().setDocumentation(DocumentationRepository.getDocumentation(super.getDevice().getId()));
        super.getDevice().setState(new ActiveState(super.getDevice()));
    }


    @Override
    public EState getState() {
        return EState.BROKEN;
    }



}
