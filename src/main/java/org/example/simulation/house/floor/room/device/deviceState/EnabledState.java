package org.example.simulation.house.floor.room.device.deviceState;

import lombok.*;
import org.example.simulation.Timer;
import org.example.simulation.house.floor.room.device.Device;


@AllArgsConstructor
@Setter
@Getter
public class EnabledState extends DeviceState {
    public EnabledState(Device device) {
        super(device);
    }

    @Override
    public void enable() {
        System.out.println("EnabledState enable");
    }

    @Override
    public void use() {
        System.out.println("EnabledState use");
        super.getDevice().setState(new ActiveState(super.getDevice(), Timer.getTime()));
    }

    @Override
    public void disable() {
        System.out.println("EnabledState disable");
        super.getDevice().setState(new DisabledState(super.getDevice()));
    }

    @Override
    public void repair() {
        System.out.println("EnabledState repair");
    }


    @Override
    public void update(int time) {
        System.out.println("EnabledState update");
    }

    @Override
    public EState getState() {
        return EState.ENABLED;
    }

}
