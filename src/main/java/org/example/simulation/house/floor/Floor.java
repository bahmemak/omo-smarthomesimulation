package org.example.simulation.house.floor;

import lombok.*;
import org.example.simulation.house.House;
import org.example.simulation.house.floor.room.Room;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Floor {
    private List<Room> rooms = new ArrayList<>();

    private House house;

    private EFloor type;
}
