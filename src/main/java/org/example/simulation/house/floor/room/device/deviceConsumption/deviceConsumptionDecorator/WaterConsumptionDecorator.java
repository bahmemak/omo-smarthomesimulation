package org.example.simulation.house.floor.room.device.deviceConsumption.deviceConsumptionDecorator;

import lombok.*;
import org.example.simulation.dto.resourceConsumption.ResourceConsumptionPerState;
import org.example.simulation.house.floor.room.device.IDevice;
import org.example.simulation.report.ConsumptionReport;

@AllArgsConstructor
@Setter
@Getter
public class WaterConsumptionDecorator extends BaseConsumptionDecorator {

    public WaterConsumptionDecorator(IDevice device, ResourceConsumptionPerState resourceConsumptionPerState) {
        super(device, resourceConsumptionPerState);
    }

    @Override
    public void update(int time) {
        System.out.println("WaterConsumptionDecorator update");

        int consumption = 0;

        switch (super.getDevice().getState()) {
            case ENABLED:
                consumption = super.getResourceConsumptionPerState().getConsumptionInEnabledState();
                break;
            case ACTIVE:
                consumption = super.getResourceConsumptionPerState().getConsumptionInActiveState();
                break;
            case DISABLED:
                consumption = super.getResourceConsumptionPerState().getConsumptionInDisabledState();
                break;
            case BROKEN:
                consumption = super.getResourceConsumptionPerState().getConsumptionInBrokenState();
                break;
        }

        ConsumptionReport.getInstance().addWaterConsumption(getId(), consumption);
        super.getDevice().update(time);
    }
}
