package org.example.simulation.house.floor.room;

import lombok.*;
import org.example.simulation.house.floor.Floor;
import org.example.simulation.house.floor.room.device.Device;
import org.example.simulation.house.floor.room.device.IDevice;
import org.example.simulation.house.floor.room.sensor.Sensor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Room {
    private List<Sensor> sensors = new ArrayList<>();
    private List<IDevice> devices = new ArrayList<>();
    private List<Human> humans = new ArrayList<>();

    private Floor floor;

    private String name;
}
