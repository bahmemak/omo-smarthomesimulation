package org.example.simulation.house.floor.room.sensor;

public enum ESensor {
    WIND,
    TEMPERATURE,
    LIGHT,
    MOTION,
    POWER
}
