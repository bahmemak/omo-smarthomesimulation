package org.example.simulation.house.floor.room.device.deviceConsumption.deviceConsumptionDecorator;

import lombok.*;
import org.example.simulation.dto.resourceConsumption.ResourceConsumptionPerState;
import org.example.simulation.house.floor.room.device.EDevice;
import org.example.simulation.house.floor.room.device.IDevice;
import org.example.simulation.house.floor.room.device.deviceState.EState;
import org.example.simulation.report.ConsumptionReport;

@NoArgsConstructor
@Setter
@Getter
public class BaseConsumptionDecorator implements IDevice {
    private IDevice device;

    private ResourceConsumptionPerState resourceConsumptionPerState;

    public BaseConsumptionDecorator(IDevice device, ResourceConsumptionPerState resourceConsumptionPerState) {
        this.device = device;
        this.resourceConsumptionPerState = resourceConsumptionPerState;
    }

    @Override
    public void update(int time) {
        System.out.println("BaseConsumptionDecorator update");

        int consumption = 0;

        switch (device.getState()) {
            case ENABLED:
                consumption = resourceConsumptionPerState.getConsumptionInEnabledState();
                break;
            case ACTIVE:
                consumption = resourceConsumptionPerState.getConsumptionInActiveState();
                break;
            case DISABLED:
                consumption = resourceConsumptionPerState.getConsumptionInDisabledState();
                break;
            case BROKEN:
                consumption = resourceConsumptionPerState.getConsumptionInBrokenState();
                break;
        }

        ConsumptionReport.getInstance().addDurabilityConsumption(getId(), consumption);
        device.update(time);
    }

    @Override
    public void enable() {
        System.out.println("BaseConsumptionDecorator enable");
        device.enable();
    }

    @Override
    public void use() {
        System.out.println("BaseConsumptionDecorator use");
        device.use();
    }

    @Override
    public void disable() {
        System.out.println("BaseConsumptionDecorator disable");
        device.disable();
    }

    @Override
    public void repair() {
        System.out.println("BaseConsumptionDecorator repair");
        device.repair();
    }

    @Override
    public int getId() {
        return device.getId();
    }

    @Override
    public EState getState() {
        return device.getState();
    }

    @Override
    public EDevice getType() {
        return device.getType();
    }

    @Override
    public boolean isIntractableWithHumans() {
        return device.isIntractableWithHumans();
    }

    @Override
    public String getDeviceName() {
        return device.getDeviceName();
    }
}
