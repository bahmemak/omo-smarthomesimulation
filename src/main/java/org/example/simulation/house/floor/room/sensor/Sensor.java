package org.example.simulation.house.floor.room.sensor;

import lombok.*;
import org.example.simulation.house.floor.room.Room;
import org.example.simulation.util.RandomGenerator;
import org.example.simulation.TimerSubscriber;
import org.example.simulation.event.EventManager;
import org.example.simulation.event.eventCommand.EventCommand;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public abstract class Sensor implements TimerSubscriber {
    private int id;

    private EventCommand command;

    @Override
    public void update(int time) {
        if (RandomGenerator.generateRandomNumber(0, 100) < 50) {
            EventManager.addEventCommand(command);
        }
    }
}
