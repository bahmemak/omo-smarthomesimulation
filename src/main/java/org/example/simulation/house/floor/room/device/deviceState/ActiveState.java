package org.example.simulation.house.floor.room.device.deviceState;

import lombok.*;
import org.example.simulation.house.floor.room.device.Device;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class ActiveState extends DeviceState {

    private int tempTime;

    public ActiveState(Device device) {
        super(device);
    }

    public ActiveState(Device device, int currentTime) {
        super(device);
        this.tempTime = currentTime;
    }

    @Override
    public void update(int time) {
        System.out.println("ActiveState update");
        if (time - tempTime == super.getDevice().getDurationOfUse()) {
            super.getDevice().setState(new EnabledState(super.getDevice()));
        }
    }

    @Override
    public void enable() {
        System.out.println("ActiveState enable");
    }

    @Override
    public void use() {
        System.out.println("ActiveState use");
    }

    @Override
    public void disable() {
        System.out.println("ActiveState disable");
        super.getDevice().setState(new DisabledState(super.getDevice()));
    }

    @Override
    public void repair() {
        System.out.println("ActiveState repair");
    }


    @Override
    public EState getState() {
        return EState.ACTIVE;
    }



}
