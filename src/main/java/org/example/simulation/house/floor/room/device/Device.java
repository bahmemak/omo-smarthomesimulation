package org.example.simulation.house.floor.room.device;

import lombok.*;
import org.example.simulation.dto.Documentation;
import org.example.simulation.house.floor.room.device.deviceState.DeviceState;
import org.example.simulation.house.floor.room.device.deviceState.EState;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Device implements IDevice {
    @Setter
    private DeviceState state;

    boolean intractableWithHumans;

    private int durationOfUse;

    private int id;

    private String deviceName;

    private EDevice type;

    private Documentation documentation;

    public Device(DeviceState initialState) {
        this.state = initialState;
        initialState.setDevice(this);
    }

    @Override
    public void enable() {
        state.enable();
    }

    @Override
    public void use() {
        state.use();
    }

    @Override
    public void disable() {
        state.disable();
    }

    @Override
    public void repair() {
        state.repair();
    }

    @Override
    public void update(int time) {
        state.update(time);
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public EState getState() {
        return state.getState();
    }

    @Override
    public EDevice getType() {
        return type;
    }

    @Override
    public String getDeviceName() {
        return deviceName;
    }
}
