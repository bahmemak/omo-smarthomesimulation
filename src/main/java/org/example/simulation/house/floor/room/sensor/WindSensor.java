package org.example.simulation.house.floor.room.sensor;

import lombok.*;
import org.example.simulation.util.RandomGenerator;
import org.example.simulation.event.EventManager;
import org.example.simulation.event.eventCommand.EventCommand;
import org.example.simulation.event.eventCommand.UseCommand;

@AllArgsConstructor
@Setter
@Getter
public class WindSensor extends Sensor {
}
