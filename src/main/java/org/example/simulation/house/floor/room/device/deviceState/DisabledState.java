package org.example.simulation.house.floor.room.device.deviceState;

import lombok.*;
import org.example.simulation.house.floor.room.device.Device;

@AllArgsConstructor
@Setter
@Getter
public class DisabledState extends DeviceState {

    public DisabledState(Device device) {
        super(device);
    }

    @Override
    public void enable() {
        System.out.println("DisabledState enable");
        super.getDevice().setState(new EnabledState(super.getDevice()));
    }

    @Override
    public void use() {
        System.out.println("DisabledState use");
    }

    @Override
    public void disable() {
        System.out.println("DisabledState disable");
    }

    @Override
    public void repair() {
        System.out.println("DisabledState repair");
    }


    @Override
    public void update(int time) {
        System.out.println("DisabledState update");
    }

    @Override
    public EState getState() {
        return EState.DISABLED;
    }


}
