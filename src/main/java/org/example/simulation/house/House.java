package org.example.simulation.house;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.example.simulation.house.floor.Floor;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class House {
    private List<Floor> floors = new ArrayList<>();

}
