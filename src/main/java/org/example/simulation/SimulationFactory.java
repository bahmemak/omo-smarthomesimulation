package org.example.simulation;

import org.example.simulation.dto.configuration.*;
import org.example.simulation.dto.resourceConsumption.ResourceConsumptionPerState;
import org.example.simulation.event.EventManager;
import org.example.simulation.event.eventCommand.UseCommand;
import org.example.simulation.house.House;
import org.example.simulation.house.floor.Floor;
import org.example.simulation.house.floor.room.Human;
import org.example.simulation.house.floor.room.Room;
import org.example.simulation.house.floor.room.device.Device;
import org.example.simulation.house.floor.room.device.EDevice;
import org.example.simulation.house.floor.room.device.IDevice;
import org.example.simulation.house.floor.room.device.deviceConsumption.EResourceType;
import org.example.simulation.house.floor.room.device.deviceConsumption.deviceConsumptionDecorator.BaseConsumptionDecorator;
import org.example.simulation.house.floor.room.device.deviceConsumption.deviceConsumptionDecorator.ElectricityConsumptionDecorator;
import org.example.simulation.house.floor.room.device.deviceConsumption.deviceConsumptionDecorator.GasConsumptionDecorator;
import org.example.simulation.house.floor.room.device.deviceConsumption.deviceConsumptionDecorator.WaterConsumptionDecorator;
import org.example.simulation.house.floor.room.device.deviceState.*;
import org.example.simulation.house.floor.room.sensor.*;
import org.example.simulation.report.ConsumptionReport;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SimulationFactory {
    public static Simulation createSimulation(SimulationConfiguration configuration) {
        int id = 1;

        ConsumptionReport consumptionReport = ConsumptionReport.getInstance();

        List<IDevice> createdDevices = new ArrayList<>();

        List<TimerSubscriber> timerSubscribers = new ArrayList<>();

        Simulation simulation = new Simulation(configuration.getDuration());

        House house = new House();

        for (FloorConfiguration floorConfiguration : configuration.getFloors()) {
            Floor floorToCreate = new Floor();
            floorToCreate.setType(floorConfiguration.getType());


            for (RoomConfiguration roomConfiguration : floorConfiguration.getRooms()) {
                Room roomToCreate = new Room();
                roomToCreate.setName(roomConfiguration.getName());

                for (DeviceConfiguration deviceConfiguration : roomConfiguration.getDevices()) {
                    Device deviceToCreate = new Device();
                    deviceToCreate.setId(id++);
                    consumptionReport.addDevice(deviceToCreate.getId());
                    deviceToCreate.setDeviceName(deviceConfiguration.getName());
                    deviceToCreate.setDurationOfUse(deviceConfiguration.getDurationOfUse());

                    switch (deviceConfiguration.getInitialState()) {
                        case ENABLED:
                            DeviceState enabledState = new EnabledState(deviceToCreate);
                            deviceToCreate.setState(enabledState);
                            break;
                        case DISABLED:
                            DeviceState disabledState = new DisabledState(deviceToCreate);
                            deviceToCreate.setState(disabledState);
                            break;
                        case ACTIVE:
                            DeviceState activeState = new ActiveState(deviceToCreate, 0);
                            deviceToCreate.setState(activeState);
                            break;
                        case BROKEN:
                            DeviceState brokenState = new BrokenState(deviceToCreate);
                            deviceToCreate.setState(brokenState);
                            break;
                    }

                    IDevice deviceWithDecorators = new BaseConsumptionDecorator(deviceToCreate, new ResourceConsumptionPerState(0, 1, 0, 2));

                    switch (deviceConfiguration.getType()) {
                        case WINDOW:
                            deviceToCreate.setIntractableWithHumans(false);
                            deviceToCreate.setType(EDevice.WINDOW);

                            deviceWithDecorators = new ElectricityConsumptionDecorator(deviceWithDecorators, new ResourceConsumptionPerState(0, 1, 0, 2));

                            break;
                        case HEATER:
                            deviceToCreate.setIntractableWithHumans(false);
                            deviceToCreate.setType(EDevice.HEATER);

                            deviceWithDecorators = new ElectricityConsumptionDecorator(deviceWithDecorators, new ResourceConsumptionPerState(0, 1, 0, 2));
                            deviceWithDecorators = new GasConsumptionDecorator(deviceWithDecorators, new ResourceConsumptionPerState(0, 1, 0, 2));

                            break;
                        case CONDITIONER:
                            deviceToCreate.setIntractableWithHumans(false);
                            deviceToCreate.setType(EDevice.CONDITIONER);

                            deviceWithDecorators = new ElectricityConsumptionDecorator(deviceWithDecorators, new ResourceConsumptionPerState(0, 1, 0, 2));
                            break;
                        case LIGHT:
                            deviceToCreate.setIntractableWithHumans(false);
                            deviceToCreate.setType(EDevice.LIGHT);

                            deviceWithDecorators = new ElectricityConsumptionDecorator(deviceWithDecorators, new ResourceConsumptionPerState(0, 1, 0, 2));
                            break;
                        case COFFEE_MACHINE:
                            deviceToCreate.setIntractableWithHumans(true);
                            deviceToCreate.setType(EDevice.COFFEE_MACHINE);

                            deviceWithDecorators = new ElectricityConsumptionDecorator(deviceWithDecorators, new ResourceConsumptionPerState(0, 1, 0, 2));
                            deviceWithDecorators = new WaterConsumptionDecorator(deviceWithDecorators, new ResourceConsumptionPerState(0, 1, 0, 2));
                            break;
                        case OVEN:
                            deviceToCreate.setIntractableWithHumans(true);
                            deviceToCreate.setType(EDevice.OVEN);

                            deviceWithDecorators = new ElectricityConsumptionDecorator(deviceWithDecorators, new ResourceConsumptionPerState(0, 1, 0, 2));
                            deviceWithDecorators = new GasConsumptionDecorator(deviceWithDecorators, new ResourceConsumptionPerState(0, 1, 0, 2));
                            break;
                        case TV:
                            deviceToCreate.setIntractableWithHumans(true);
                            deviceToCreate.setType(EDevice.TV);

                            deviceWithDecorators = new ElectricityConsumptionDecorator(deviceWithDecorators, new ResourceConsumptionPerState(0, 2, 0, 2));

                            break;
                        case VACUUM_CLEANER:
                            deviceToCreate.setIntractableWithHumans(true);
                            deviceToCreate.setType(EDevice.VACUUM_CLEANER);

                            deviceWithDecorators = new ElectricityConsumptionDecorator(deviceWithDecorators, new ResourceConsumptionPerState(0, 1, 0, 2));
                            break;
                    }

                    createdDevices.add(deviceWithDecorators);
                    roomToCreate.getDevices().add(deviceWithDecorators);

                    timerSubscribers.add(deviceWithDecorators);
                }

                for (SensorConfiguration sensorConfiguration : roomConfiguration.getSensors()) {
                    switch (sensorConfiguration.getType()) {
                        case TEMPERATURE:
                            Sensor temperatureSensor = new TemperatureSensor();

                            temperatureSensor.setId(id++);

                            UseCommand useCommand = new UseCommand();

                            useCommand.setDevices(createdDevices.stream().filter(device -> device.getType() == EDevice.HEATER).collect(Collectors.toList()));

                            temperatureSensor.setCommand(useCommand);

                            roomToCreate.getSensors().add(temperatureSensor);

                            timerSubscribers.add(temperatureSensor);
                            break;
                        case LIGHT:
                            Sensor lightSensor = new LightSensor();

                            lightSensor.setId(id++);

                            UseCommand useCommand1 = new UseCommand();

                            useCommand1.setDevices(createdDevices.stream().filter(device -> device.getType() == EDevice.LIGHT).collect(Collectors.toList()));

                            lightSensor.setCommand(useCommand1);

                            roomToCreate.getSensors().add(lightSensor);

                            timerSubscribers.add(lightSensor);

                            break;

                        case WIND:
                            Sensor windSensor = new WindSensor();

                            windSensor.setId(id++);

                            UseCommand useCommand2 = new UseCommand();

                            useCommand2.setDevices(createdDevices.stream().filter(device -> device.getType() == EDevice.WINDOW).collect(Collectors.toList()));

                            windSensor.setCommand(useCommand2);

                            roomToCreate.getSensors().add(windSensor);

                            timerSubscribers.add(windSensor);

                            break;

                        case MOTION:
                            Sensor motionSensor = new MotionSensor();

                            motionSensor.setId(id++);

                            UseCommand useCommand3 = new UseCommand();
                            useCommand3.getDevices().addAll(
                                    roomToCreate.getDevices().stream()
                                            .filter(device -> device.getType() == EDevice.LIGHT)
                                            .collect(Collectors.toList()));

                            motionSensor.setCommand(useCommand3);

                            roomToCreate.getSensors().add(motionSensor);

                            timerSubscribers.add(motionSensor);

                            break;
                    }
                }

                for (HumanConfiguration humanConfiguration : roomConfiguration.getHumans()) {
                    Human human = new Human();
                    human.setName(humanConfiguration.getName());

                    roomToCreate.getHumans().add(human);
                    human.setRoom(roomToCreate);

                    timerSubscribers.add(human);
                }

                roomToCreate.setFloor(floorToCreate);
                floorToCreate.getRooms().add(roomToCreate);
            }
            floorToCreate.setHouse(house);
            house.getFloors().add(floorToCreate);
        }


        EventManager ev = new EventManager();
        simulation.setEventManager(ev);
        Timer timer = new Timer(timerSubscribers);
        timer.subscribe(ev);
        simulation.setTimer(timer);

        return simulation;
    }
}
