package org.example.simulation;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;


public class Timer {
    @Getter
    private static int time = 0;

    private List<TimerSubscriber> subscribers = new ArrayList<>();

    public Timer(List<TimerSubscriber> subscribers) {
        this.subscribers = subscribers;
    }

    public void subscribe(TimerSubscriber subscriber) {
        subscribers.add(subscriber);
    }

    public void unsubscribe(TimerSubscriber subscriber) {
        subscribers.remove(subscriber);
    }

    public void notifySubscribers(int time) {
        for (TimerSubscriber subscriber : subscribers) {
            subscriber.update(time);
        }
    }

    protected void incrementTime() {
        time++;
        notifySubscribers(time);
    }
}
