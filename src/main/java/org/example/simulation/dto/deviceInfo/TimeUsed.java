package org.example.simulation.dto.deviceInfo;

public class TimeUsed {
    private int timeUsedActiveState;
    private int timeUsedEnabledState;
    private int timeUsedDisabledState;
    private int timeUsedBrokenState;
}
