package org.example.simulation.dto.report;

import lombok.Data;
import org.example.simulation.house.floor.room.Human;

import java.util.List;
import java.util.Map;

@Data
public class ActivityAndUsage {
    private Human human;

    private Map<Integer, Integer> deviceUsage;
}
