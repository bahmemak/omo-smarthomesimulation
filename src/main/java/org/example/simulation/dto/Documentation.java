package org.example.simulation.dto;

import lombok.Data;

@Data
public class Documentation {
    private String text;
}
