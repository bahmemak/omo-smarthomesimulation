package org.example.simulation.dto.configuration;

import lombok.Data;
import lombok.ToString;
import org.example.simulation.dto.resourceConsumption.ResourceConsumptionPerState;
import org.example.simulation.house.floor.room.device.deviceConsumption.EResourceType;

import java.util.Map;

@Data
@ToString
public class ResourceConsumptionConfiguration {
    private Map<EResourceType, ResourceConsumptionPerState> resourceConsumptionPerState;
}
