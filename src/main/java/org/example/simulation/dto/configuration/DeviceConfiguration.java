package org.example.simulation.dto.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.example.simulation.house.floor.room.device.EDevice;
import org.example.simulation.house.floor.room.device.deviceState.EState;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DeviceConfiguration {
    @JsonProperty("type")
    EDevice type;

    @JsonProperty("durationOfUse")
    private int durationOfUse;

    @JsonProperty("name")
    private String name;

    @JsonProperty("initialState")
    private EState initialState;
}
