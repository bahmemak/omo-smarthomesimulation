package org.example.simulation.dto.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SimulationConfiguration {
    @JsonProperty("duration")
    private int duration;

    @JsonProperty("floors")
    private List<FloorConfiguration> floors;
}
