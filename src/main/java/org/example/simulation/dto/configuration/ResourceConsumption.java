package org.example.simulation.dto.configuration;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ResourceConsumption {
    private int durability;
    private int powerConsumption;
    private int waterConsumption;
    private int gasConsumption;
}
