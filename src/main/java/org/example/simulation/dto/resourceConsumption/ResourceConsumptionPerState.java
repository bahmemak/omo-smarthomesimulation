package org.example.simulation.dto.resourceConsumption;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResourceConsumptionPerState {
    private int consumptionInDisabledState;
    private int consumptionInEnabledState;
    private int consumptionInBrokenState;
    private int consumptionInActiveState;
}
