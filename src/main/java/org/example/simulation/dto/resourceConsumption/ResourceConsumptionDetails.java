package org.example.simulation.dto.resourceConsumption;

import lombok.Data;

@Data
public class ResourceConsumptionDetails {
    private int durabilityConsumption = 0;
    private int electricityConsumption = 0;
    private int waterConsumption = 0;
    private int gasConsumption = 0;
}
