package org.example.simulation.event.eventCommand;

public interface EventCommand {
    boolean execute();
}
