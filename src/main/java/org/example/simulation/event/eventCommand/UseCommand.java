package org.example.simulation.event.eventCommand;

import lombok.*;
import org.example.simulation.house.floor.room.device.IDevice;
import org.example.simulation.house.floor.room.device.deviceState.EState;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class UseCommand implements EventCommand {
    private List<IDevice> devices = new ArrayList<>();

    private List<IDevice> tempDevices = new ArrayList<>();

    @Override
    public boolean execute() {
        if (tempDevices.isEmpty()) {
            tempDevices = devices;
        }

        for (int i = 0; i < tempDevices.size(); i++) {
            if (tempDevices.get(i).getState().equals(EState.ENABLED)) {
                tempDevices.get(i).use();
                tempDevices.remove(devices.get(i));
            }
        }

        if (tempDevices.isEmpty()) {
            System.out.println("Use command executed");
            return true;
        } else {
            System.out.println("Some device is not enabled");
            return false;
        }
    }
}
