package org.example.simulation.event.eventCommand;

import lombok.*;
import org.example.simulation.house.floor.room.device.IDevice;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class DisableCommand implements EventCommand {
    private List<IDevice> devices = new ArrayList<>();

    @Override
    public boolean execute() {
        for (IDevice device : devices) {
            device.disable();
        }
        System.out.println("Disable command executed");
        return true;
    }
}
