package org.example.simulation.event;

import lombok.*;
import org.example.simulation.TimerSubscriber;
import org.example.simulation.event.eventCommand.EventCommand;

import java.util.LinkedList;
import java.util.List;

@AllArgsConstructor
@Setter
@Getter
public class EventManager implements TimerSubscriber {
    @Getter
    @Setter
    private static LinkedList<EventCommand> eventCommands = new LinkedList<>();

    public static void addEventCommand(EventCommand eventCommand) {
        eventCommands.add(eventCommand);
    }

    @Override
    public void update(int time) {
        if (eventCommands.isEmpty()) {
            return;
        }

        EventCommand eventCommand = eventCommands.pop();
        if (!eventCommand.execute()) {
            eventCommands.push(eventCommand);
        }
    }
}
