package org.example;

import org.example.simulation.Simulation;
import org.example.simulation.SimulationFactory;
import org.example.simulation.dto.configuration.*;
import org.example.simulation.house.floor.EFloor;
import org.example.simulation.house.floor.room.device.EDevice;
import org.example.simulation.house.floor.room.device.deviceState.EState;
import org.example.simulation.house.floor.room.sensor.ESensor;
import org.example.simulation.util.ConfigurationLoader;
import org.example.simulation.util.ReportExportUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class App {
    public static void main( String[] args ) {
        SimulationConfiguration simulationConfiguration = ConfigurationLoader.loadConfiguration("src/main/resources/configuration.json");
        
        Simulation simulation = SimulationFactory.createSimulation(simulationConfiguration);

        ReportExportUtil.exportReport(simulationConfiguration.toString(), "simulationConfiguration.json");

        simulation.run();

    }
}
