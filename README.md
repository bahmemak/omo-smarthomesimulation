# Smart Home Simulation Project

Welcome to the Smart Home Simulation project! This simulation provides a realistic environment to explore the interactions among devices, sensors, and people in a smart home setting. Devices automatically respond to signals from sensors, and people can interact with the devices. Each device consumes specific resources based on its state. The simulation is configured using a JSON file, and the results are available in the form of Report files.

![](https://i.ibb.co/JdMM7C3/Smart-Home-Simulation.png)

## Features

- **Simulation Objects:**
  - Devices: Smart appliances that automatically respond to sensor signals.
  - Sensors: Devices that capture signals and trigger device responses.
  - People: Residents who can interact with devices.

- **Resource Consumption:**
  - Devices consume resources based on their states.

- **Configuration:**
  - Simulation configurations are specified in a JSON file.

- **Results:**
  - Simulation results are available in Report files.

## Functional requirements
- **F1:**
  - The entities we work with are a house, a floor in a house, a room, sensors, a device (=appliance), a person. (It is in the directory org/example/simulation/house )
- **F2:**
  - Individual devices in the house have an API to control them. Done via state machine Devices have a state that can be changed using an API to control it. Actions from the API are applicable based on the state of the device. (It is in the catalog org/example/simulation/house/floor/room/device/deviceState )
- **F3:**
  - Appliances have their consumption in active state, idle state, switched off state. We used a decorator for this. (It is in the catalog org/example/simulation/house/floor/room/device/deviceConsumption/deviceConsumptionDecorator )
- **F4:**
  - Via the API, we determine the status of the device, and information about consumption is stored and collected in a report. (It is in the org/example/simulation/report directory)
- **F5:**
  - People in the simulation perform actions that affect the state of the device, such as the wearability of the device.
- **F6:**
  - Individual devices and people are present in the same room at any moment and randomly generate events.
- **F7:**
  - Events are received and processed by a suitable person or facility. The connection between the sensor and the device is implemented using the Command pattern.
- **F8:**
  - Generating reports. A singleton is used here.
- **F9:**
  -When the device is broken, the resident of the house can repair it. Lazy Loading is used here.



## Design Patterns

The project incorporates several design patterns to enhance its structure and maintainability:

- **Reports (Singleton):**
  - Utilizes the Singleton pattern to manage and provide access to Report instances.

- **Sensors to Devices (Command):**
  - Implements the Command pattern for the communication between sensors and devices.

- **Devices Consumption (Decorator):**
  - Applies the Decorator pattern to manage the consumption of resources by devices.

- **Devices States (State Machine):**
  - Utilizes the State Machine pattern to handle the various states of devices.

- **Device Documentation (Lazy Loading):**
  - Adopts lazy loading to efficiently load and manage device documentation.

- **Simulation Creation (Factory):**
  - Implements the Factory pattern for the creation of simulation instances.

